//create my function here
//I'll pass my object through so I can access the userID
function createShoppingList(userObject) {
  const input = require('readline-sync');
  //create a blank object with userID and an array for the shoppingList
  let shoppingListObject = {
    shopperID: userObject.uId,
    shoppingList: []
  };
  let shoppingItem = input.question("What would you like to add to your shopping list? ");

  while (shoppingItem.toLowerCase() != 'stop') {
    shoppingListObject.shoppingList.push(shoppingItem);
    shoppingItem = input.question("What else would you like to add to your shopping list? ");
  }
  return shoppingListObject;
}

module.exports = createShoppingList;
