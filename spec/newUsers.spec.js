/*
Users should be stored as objects
Users should have:
>>>First Name
>>>Last Name
>>>UserName
>>>Password
>>>ID
>>>
*/
const userChecks = require('../newUsers.js');
const assert = require('assert');

describe("User Output", function() {
  it("Returns an object when passed an array of user entries", function() {
    let newUserTest = userChecks(["John", "Doe", "jdoe", "12345"]);
    let newUserCheck = { fName: "John", lName: "Doe", uName: "jdoe", uPass: "12345", uId: 5 };
    assert.deepEqual(newUserTest, newUserCheck);    //can't use strictEqual with objects
  });
});
