/*
Existing users are objects that are stored in an array of users
Existing users are asked for their userID and their password
Both arrays are passed to our function as parameters and,
if valid, the user is allowed in to the program and the existing user object
is returned.
If invalid, the program breaks.
*/

const userLogIn = require('../existingUser.js');
const assert = require('assert');

describe("Existing user log in", function() {
  it("Checks for a valid & matching existing user, see comments above", function() {

    //not necessarily the best way to do this, but...
    let user1 = { fName: "John", lName: "Doe", uName: "j_doe", uPass: "12345", uId: 1 };
    let user2 = { fName: "Jane", lName: "Doe", uName: "jdoe", uPass: "54321", uId: 2 };
    let user3 = { fName: "Jim", lName: "Doe", uName: "jimdoe", uPass: "12345", uId: 3 };
    let user4 = { fName: "Justine", lName: "Doe", uName: "judo", uPass: "12345", uId: 4 };

    const existingUserArray = [user1, user2, user3, user4];

    let existingUserTest = userLogIn(["j_doe", "12345"], existingUserArray);
    let existingUserCheck = { fName: "John", lName: "Doe", uName: "j_doe", uPass: "12345", uId: 1 };
    assert.deepEqual(existingUserTest, existingUserCheck);
  });
});
