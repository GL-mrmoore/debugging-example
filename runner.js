const input = require('readline-sync');
const newUserFunctions = require('./newUsers.js');
const existingUserFunctions = require('./existingUser.js');
const goShopping = require('./shoppingList.js');

function welcomeMenu() {
  console.log("Welcome");
  console.log("Please enter 1 if you are a new user");
  console.log("Please enter 2 if you are an existing user");
  let menuChoice = input.question("Please enter where you would like to go: ");

  return menuChoice;
}

function newUserMenu() {
  let newUserArray = [];

  let newUserFirstName = input.question("Please enter your first name: ");
  newUserArray.push(newUserFirstName);

  let newUserLastName = input.question("Please enter your last name: ");
  newUserArray.push(newUserLastName);

  let newUserId = input.question("Please enter your user ID: ");
  newUserArray.push(newUserId);

  let newUserPassword = input.question("Please enter the password you would like to use: ");
  newUserArray.push(newUserPassword);

  return newUserFunctions(newUserArray);
}

function existingUserMenu() {
  /* going to have to cheat somewhat here, I'm going to create a default list
     of existing users since we don't have persistence in our data yet */

  let user1 = { fName: "John", lName: "Doe", uName: "j_doe", uPass: "12345", uId: 1 };
  let user2 = { fName: "Jane", lName: "Doe", uName: "jdoe", uPass: "54321", uId: 2 };
  let user3 = { fName: "Jim", lName: "Doe", uName: "jimdoe", uPass: "12345", uId: 3 };
  let user4 = { fName: "Justine", lName: "Doe", uName: "judo", uPass: "12345", uId: 4 };

  const existingUserArray = [user1, user2, user3, user4];
  let checkExistingUser = [];

  let existingUserId = input.question("Please enter your username: ");
  checkExistingUser.push(existingUserId);

  let existingUserPassword = input.question("Please enter your password: ");
  checkExistingUser.push(existingUserPassword);

  return existingUserFunctions(checkExistingUser, existingUserArray);
}

function createShoppingList() {

}

function runProgram() {
  let userFirstChoice = welcomeMenu();
  if (userFirstChoice == 1) {
    let x = newUserMenu();
    console.log(`Welcome ${x.fName} ${x.lName}`);
    let x_list = goShopping(x);
    console.log(x_list.shoppingList);
  } else if (userFirstChoice == 2) {
    let y = existingUserMenu()
    console.log(`Welcome ${y.fName} ${y.lName}`);
    let y_list = goShopping(y);
    console.log(y_list.shoppingList);
  } else {
    console.clear();
    console.log(`${userFirstChoice} was not a valid entry. Please try again`);
    welcomeMenu();
  }
}

runProgram();
